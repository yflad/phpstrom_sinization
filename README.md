# PhpStrom 汉化
闲暇时做了个汉化，自己纯手工。准备分为两个版本，一个为setting(设置)的汉化，另一个为全汉化，目前只完成了setting 的部分汉化，待有时间再一点点添加
其中错误不可避免，如果有兴趣可以纠正下，不胜感激！

# 使用方法
克隆全部文件，保留 .properties 文件。在PhpStrom 安装目录下的 lib 目录，有个 resources_en.jar
用解压缩软件直接打开
将 .properties 全部复制到messages 目录下

![输入图片说明](https://gitee.com/uploads/images/2018/0322/115557_62f60c1f_993479.png "屏幕截图.png")

【注意备份，注意备份，注意备份】
